# Install Arch Linux on an EFI system with LVM luks encryption. 


>By: zer0the0ry


----


## Assumptions:


 * You have an Arch Linux USB thumb drive made from the official .iso file.
 * You have an EFI system.
 * You are using an Intel CPU and NVIDIA GPU. 


### What you will get: 


A full Arch Linux LUKS encrypted system with the xfce4 desktop environment.


----


## Sources


[Arch Linux Wiki](https://wiki.archlinux.org/index.php/Installation_guide)

[D3S0X](https://github.com/D3S0X/arch-installation)

[LearnLinuxTV](https://youtu.be/Lq4cbp5AOZM)

[eznix](https://youtu.be/fMYmhGCKLoE)


## Internet


Ensuring your network interface is listed and enabled. You cannot complete the installation without a internet connection.


```
ip link
```


To connect with wifi you will need to do these following steps.


```
iwctl
device list

```

Find your wifi interface from the list. Usually will be wlan0. If it is something else, replace wlan0 with your interface device name in the following commands.


```
station wlan0 scan
station wlan0 get-networks

```

You should see your wifi SSID within the list. 


```
station wlan0 connect <SSID>
exit
```

You should be connected to the internet.


Check for internet connection.


```
ping -c 3 archlinux.org 
```


## Initial Setup


Set the system clock.


```
timedatectl set-ntp true
```


Check if your booted in BIOS or UEFI.


```
ls /sys/firmware/efi/efivars
```


If not found, you have booted in BIOS mode and need reboot to change the BIOS settings to UEFI in order to use this guide correctly, otherwise, if you know what you're doing, proceed with caution. 


Refresh repositories.


```
pacman -Syy
```


## Arch keyring and reflector


Reflector is an app that will rank the mirrors according to your needs. Change the parameters to how they will best work for you such as changing your country etc.


```
pacman -Sy archlinux-keyring reflector
```


Update your mirrorlist.


```
reflector --country US --age 16 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```


## Partitioning


See storage devices of your pc (sda, sdb, nvme0n1)


NOTE: You want to pay close attention to how specifically your storage device you wish to install Arch to is listed.


For these examples, I will be using nvme0n1 as the example.


```
lsblk
```


Start the partition tool fdisk.


```
fdisk /dev/nvme0n1
```


## Inside fdisk environment


**p** to see partition layout


**g** for brand new partition layout


**n** for new


(partition number 1) press **[enter]** to select default.


(first sector question) **[enter]** again to select default.


Type **+500M** create a 500mb partition


**t** for type, auto selects partition 1


Give it number **1** for EFI system - be sure to check the list as default number for EFI could change.


**n** for a new partition


(partition number 2) press **[enter]** for default as you did above.


(first sector) **[enter]** again.


**+500M** create another partition with 500mb


**n** for new again


(partition number 3) Press **[enter]**.


(first sector) **[enter]** again.


Finally press **[enter]** to use remainder of disk space.


**t** for type


(default 3) Press enter.


Enter **31** for Linux LVM - be sure to check the list and LVM is selected as the default number could change.


**p** to see list of changes


Now your system should have:


```
500M EFI System
500M Linux filesystem
xxxG Linux LVM
```


**w** to write changes


This is the end of using fdisk.


## Back to terminal prompt


Check to see if it's configured correctly.


```
lsblk 
```


Setup the partitions.


```
mkfs.fat -F32 /dev/nvme0n1p1
```


```
mkfs.ext4 /dev/nvme0n1p2
```


## Setup Encryption


This will begin the luks encryption process. 


`cryptsetup luksFormat /dev/nvme0n1p3`


Set the passphrase then open the volume.


`cryptsetup open --type luks /dev/nvme0n1p3 lvm`


Enter your passphrase.


Create the physical volume.


`pvcreate --dataalignment 1m /dev/mapper/lvm`


Create the volume group.


`vgcreate volgroup0 /dev/mapper/lvm`


Create the logical volume.


`lvcreate -l 100%FREE volgroup0 -n lvsystem`


Format the filesystem.


`mkfs.ext4 /dev/volgroup0/lvsystem`


Mount the logical volume.


`mount /dev/volgroup0/lvsystem /mnt`


Create the boot directory.


`mkdir /mnt/boot`


Mount the boot partition (not created in lvm).


`mount /dev/nvme0n1p2 /mnt/boot`


Create the /etc directory.


`mkdir /mnt/etc`


Generate fstab.


`genfstab -U -p /mnt >> /mnt/etc/fstab`


## Pacstrap


Doing the pacstrap. This is the initial installation of packages into your system.


`pacstrap -i /mnt base base-devel linux linux-headers linux-firmware sysfsutils usbutils e2fsprogs inetutils netctl vim less which man-db man-pages lvm2 dhcpcd dialog dosfstools mtools`


Note: you will need a text editor once you are in chroot. If you're not comfortable with vim change it out in your pacstrap to nano.


## The chroot


Entering your environment.


`arch-chroot /mnt`


Time zone set.


`ln -sf /usr/share/zoneinfo/(region)/(city or zone) /etc/localtime`


`hwclock --systohc --utc`


## Configure locale


Use the editor you have chosen for this section.


Edit the **/etc/locale.gen** file.


Uncomment the line with **en_US.UTF-8**


Or select the language you use for this part of the guide.


`locale-gen`


`echo LANG=en_US.UTF-8 >> /etc/locale.conf`


`export LANG=en_US.UTF-8`


## Setup networking


Note: *yourhost* should be replaced with the name you wish to use.


`echo yourhost > /etc/hostname`


Edit the file **/etc/hosts**


Add the lines:


```
127.0.0.1 localhost
::1	localhost
127.0.1.1 yourhost.localdomain yourhost
```


## Setup multilib


Edit the file **/etc/pacman.conf**


Uncomment **Color** and **multilib**.


Add ***ILoveCandy*** to Misc.


Save and exit.


Update the system.


`pacman -Syu`


## Setup users


Set the root password.


`passwd`


Add your main user.


`useradd -m -g users -G wheel,audio,video,storage,power,input,optical,sys,log,network,scanner,rfkill,lp,adm (username)`


Set user password.


`passwd (username)`


Use visudo to edit sudoers to uncomment %wheel 


`visudo`


Uncomment the line with %wheel 


NOTE: If you prefer nano text editor use `EDITOR=nano visudo`


## Setup grub, EFI, last partition. 


Install necessary packages:


`pacman -S grub efibootmgr dosfstools os-prober mtools gptfdisk fatresize`


edit file **/etc/mkinitcpio.conf**


Find line with uncommented **HOOKS**


In between **block** and **filesystems** add **encrypt lvm2**


Save and exit.


`mkinitcpio -p linux`


edit file **/etc/default/grub**


edit line: GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 cryptdevice=/dev/nvme0n1p3:volgroup0:allow-discards quiet"


Uncomment **GRUB_ENABLE_CRYPTODISK=y**


Save and exit.


Make EFI directory and mount it.


`mkdir /boot/EFI`


`mount /dev/nvme0n1p1 /boot/EFI`


Install grub and setup.


`grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck`


`mkdir /boot/grub/locale`


`cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo`


`grub-mkconfig -o /boot/grub/grub.cfg`


## Make swap file (optional)


Follow these steps if you want a swap file. This installation does not create a swap partition.


`fallocate -l 4G /swapfile`


`chmod 600 /swapfile`


`mkswap /swapfile`


Backup the **/etc/fstab** file.


`echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab`


Sanity Check.

`cat /etc/fstab` should have 3 entries **/** , **/boot** , and **/swapfile**


## Install before reboot


Install reflector and the intel microcode. 


`pacman -S reflector intel-ucode`


Install other video and networking packages.


`pacman -S nvidia nvidia-utils nvidia-settings vulkan-intel networkmanager wpa_supplicant wireless_tools netctl openssh networkmanager-openvpn networkmanager-pptp`


Enable your system to have the internet.


`systemctl enable NetworkManager`


## Additional Packages


Install these with pacman.


**Desktop manager xfce4 and lightdm login manager**

```
pacman -S xfce4 xfce4-goodies lightdm lightdm-gtk-greeter gparted transmission-gtk pavucontrol xfburn asunder quodlibet xarchiver arc-gtk-theme arc-icon-theme adapta-gtk-theme polkit-gnome gnome-disk-utility gnome-packagekit lightdm-gtk-greeter-settings
```


Enable lightdm or your system ***will boot to a black screen***.


`systemctl enable lightdm`


**video**


xorg xorg-server xorg-xinit xorg-xrandr xorg-xfontsel xorg-xkill
xterm xorg-drivers mesa lib32-mesa vulkan-icd-loader lib32-vulkan-icd-loader lib32-nvidia-utils 


**utils**


dkms p7zip haveged pacman-contrib git diffutils logrotate mdadm perl ntfs-3g exfat-utils gvfs xdg-user-dirs xscreensaver htop rsync lsb-release polkit packagekit device-mapper jshon expac wget acpid


**audio and video**


pulseaudio vlc simplescreenrecorder cdrtools gstreamer gst-libav gst-plugins-base gst-plugins-base-libs gst-plugins-good gst-plugins-bad gst-plugins-ugly gstreamer-vaapi xvidcore frei0r-plugins cdrdao dvdauthor transcode ffmpeg libdvdcss gimp guvcview imagemagick flac faad2 faac mpgegtools x265 x264 lame


**internet**


nm-connection-editor network-manager-applet avahi openresolv youtube-dl


**fonts**


ttf-ubuntu-font-family ttf-dejavu ttf-bitstream-vera ttf-liberation noto-fonts ttf-opensans opendesktop-fonts freetype2


**printer**


system-config-printer foomatic-db foomatic-db-engine gutenprint cups cups-pdf cups-filers cups-pk-helper ghostscript gsfonts


## Enable Services


`systemctl enable --now fstrim.timer`


Reboot and enjoy.


